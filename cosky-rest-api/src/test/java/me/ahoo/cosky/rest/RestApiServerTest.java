package me.ahoo.cosky.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ahoo wang
 */
@SpringBootTest("spring.cloud.bootstrap.enabled=true")
class RestApiServerTest {
    @Test
    void contextLoads() {
    }

}

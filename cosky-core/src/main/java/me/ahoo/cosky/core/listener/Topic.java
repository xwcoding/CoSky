package me.ahoo.cosky.core.listener;

/**
 * @author ahoo wang
 */
public interface Topic {
    String getTopic();
}
